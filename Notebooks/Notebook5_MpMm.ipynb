{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "02d2942e",
   "metadata": {},
   "source": [
    "# Notebook 5 - Coupling many free particles to many photonic modes\n",
    "-------------\n",
    "Let us consider the case when we couple to many particles, i.e.,\n",
    "\n",
    "$$\n",
    "\\hat{H}^{\\text{lwa}}_{\\text{vel}} = \\frac{1}{2m} \\sum_{j=1}^{N_{e}}\\left(- \\mathrm{i} \\hbar \\partial_{x_j} + \\frac{|e|}{c} \\hat{A} \\right)^2 +\\sum_{\\alpha=1}^{M_p} \\hbar \\omega_{\\alpha} \\left(\\hat{a}^\\dagger_{\\alpha} \\hat{a}_{\\alpha} + \\frac{1}{2} \\right),\n",
    "$$\n",
    "\n",
    "with the multi-mode vector potential \n",
    "\n",
    "$$\n",
    "\\hat{A} = \\sqrt{\\tfrac{\\hbar c^2}{\\epsilon_0 L^3}}\\sum_{\\alpha=1}^{M_p} \\tfrac{1}{\\sqrt{2 \\omega_{\\alpha}}} (\\hat{a}_{\\alpha} + \\hat{a}^\\dagger_{\\alpha}) \\,.\n",
    "$$\n",
    "\n",
    "If, as discussed in <font color='blue'>A. 1.1</font>, we diagonalize the photonic part of the coupled system for many modes, we find\n",
    "\n",
    "$$\n",
    "\\hat{H}^{\\text{lwa}}_{\\text{vel}} = -\\sum_{j=1}^{N_{e}}\\left[\\frac{\\hbar^2}{2m} \\partial_{x_j}^2 + \\frac{\\mathrm{i} \\hbar \\partial_{x_j}}{m}\\frac{|e|}{c} \\hat{A}\\right] + N_{e}\\frac{\\hat{A}^2}{2m} +\\sum_{\\alpha=1}^{M_p} \\hbar \\omega_{\\alpha} \\left(\\hat{a}^\\dagger_{\\alpha} \\hat{a}_{\\alpha} + \\frac{1}{2} \\right) = -\\sum_{j=1}^{N_{e}}\\left[\\frac{\\hbar^2}{2m} \\partial_{x_j}^2 + \\frac{\\mathrm{i} \\hbar \\partial_{x_j}}{m}\\frac{|e|}{c} \\hat{A}\\right]  +\\sum_{\\alpha=1}^{M_p} \\hbar \\tilde{\\omega}_{\\alpha} \\left(\\hat{b}^\\dagger_{\\alpha} \\hat{b}_{\\alpha} + \\frac{1}{2} \\right),\n",
    "$$\n",
    "\n",
    "and \n",
    "\n",
    "$$\n",
    "\\hat{A} = \\sqrt{\\tfrac{\\hbar c^2}{\\epsilon_0 L^3}}\\sum_{\\alpha=1}^{M_p} \\tfrac{1}{\\sqrt{2 \\tilde{\\omega}_{\\alpha}}} (\\hat{b}_{\\alpha} + \\hat{b}^\\dagger_{\\alpha}),\n",
    "$$\n",
    "\n",
    "with $\\tilde{\\omega}_{\\alpha}$ the new mode frequencies in the diagonal problem exactly as before. However, we note that the diamagnetic frequency contains the number of electrons, i.e., $\\omega_{\\text{d}} = \\sqrt{\\frac{e^2 N_e}{m \\epsilon_0 L^3}}$.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a935c95e",
   "metadata": {},
   "source": [
    "## Q. 5.1\n",
    "-------------\n",
    "At this point we can consider two different situations: (i) a large but finite number of particles in a given quantization box and (ii) an extended systems with a fixed electronic density $n_{e}=N_{e}/L^3$, where the particle number depends on the quantization volume instead. Here we look at (i) while the second case will be discussed in Lec. 2.1.\n",
    "\n",
    "Before proceeding with the diagonalization we note that the bilinear coupling is directly related to the total momentum of the electrons since,\n",
    "\n",
    "$$\n",
    "-\\sum_{j=1}^{N_{e}}\\frac{\\mathrm{i} \\hbar \\partial_{x_j}}{m}\\frac{|e|}{c} \\hat{A} = \\frac{ \\hbar }{m}\\left(\\sum_{j=1}^{N_{e}}-\\mathrm{i}\\hbar\\partial_{x_j}\\right)\\frac{|e|}{c} \\hat{A}\n",
    "$$\n",
    "\n",
    "it is then convenient to perform a center of charge transformation $\\hat{X}_{\\mathrm{coc}}=\\frac{1}{\\sqrt{N_{e}}}\\sum_{j=1}^{N_e} x_j$ whose conjugate momentum is $\\hat{P}_{\\mathrm{coc}}=\\frac{1}{\\sqrt{N_{e}}}\\sum_{j=1}^{N_e} -\\mathrm{i} \\hbar \\partial_{x_j}$, so that\n",
    "\n",
    "$$\n",
    "-\\sum_{j=1}^{N_{e}}\\frac{\\mathrm{i} \\hbar \\partial_{x_j}}{m}\\frac{|e|}{c} \\hat{A} = \\frac{|e|\\sqrt{N_e} }{mc}\\hat{P}_{\\mathrm{coc}} \\hat{A} \\; . \n",
    "$$\n",
    "\n",
    "The center of charge transformation allows you to decouple the center of charge from the relative coordinates. The center of charge Hamiltonian is then\n",
    "\n",
    "\n",
    "$$\n",
    "\\hat{H}^{\\text{lwa}}_{\\text{coc}} = \\frac{1}{2m} \\hat{P}^2_{\\mathrm{coc}} +  \\frac{|e|\\sqrt{N_e} }{mc}\\hat{P}_{\\mathrm{coc}} \\hat{A}   + \\sum_{\\alpha=1}^{M_p} \\hbar \\tilde{\\omega}_{\\alpha} \\left(\\hat{b}^\\dagger_{\\alpha} \\hat{b}_{\\alpha} + \\frac{1}{2} \\right),\n",
    "$$\n",
    "\n",
    "You can now diagonalize the Hamiltonian exactly as in the case of the single electron. However, what is the difference? Plot the renormalized mass.\n",
    "\n",
    "**Note:** The new center of charge momentum and position are conjugate variables satisfying the standard commutation relations and do not have any physical meaning apriori. Specifically, $\\hat{P}_{\\textrm{coc}}$ is not the total-momentum operator."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
