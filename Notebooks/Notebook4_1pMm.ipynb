{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "02d2942e",
   "metadata": {},
   "source": [
    "# Notebook 4 - Coupling one particle to many modes\n",
    "-------------\n",
    "Let us consider the case when we couple to many modes, i.e.,\n",
    "\n",
    "$$\n",
    "\\hat{H}^{\\text{lwa}}_{\\text{vel}} = \\frac{1}{2m} \\left(- \\mathrm{i} \\hbar \\partial_x + \\frac{|e|}{c} \\hat{A} \\right)^2 +\\sum_{\\alpha=1}^{M_p} \\hbar \\omega_{\\alpha} \\left(\\hat{a}^\\dagger_{\\alpha} \\hat{a}_{\\alpha} + \\frac{1}{2} \\right),\n",
    "$$\n",
    "\n",
    "with the multi-mode vector potential \n",
    "\n",
    "$$\n",
    "\\hat{A} = \\sqrt{\\tfrac{\\hbar c^2}{\\epsilon_0 L^3}}\\sum_{\\alpha=1}^{M_p} \\tfrac{1}{\\sqrt{2 \\omega_{\\alpha}}} (\\hat{a}_{\\alpha} + \\hat{a}^\\dagger_{\\alpha})\n",
    "$$\n",
    "\n",
    "on $L^2(\\Omega_{\\text{spatial}}) \\otimes \\mathcal{F}_1 \\otimes \\dots \\otimes \\mathcal{F}_{M_p}$. If as in <font color='blue'>A. 1.1 </font>we diagonalize the photonic part of the coupled system for many modes (this requires making the many diamagnetically coupled photonic modes diagonal, which will be explained in Lec 3.1), we find\n",
    "\n",
    "$$\n",
    "\\hat{H}^{\\text{lwa}}_{\\text{vel}} = -\\frac{\\hbar^2}{2m} \\partial_x^2 - \\frac{\\mathrm{i} \\hbar \\partial_x}{m}\\frac{|e|}{c} \\hat{A}  +\\sum_{\\alpha=1}^{M_p} \\hbar \\tilde{\\omega}_{\\alpha} \\left(\\hat{b}^\\dagger_{\\alpha} \\hat{b}_{\\alpha} + \\frac{1}{2} \\right),\n",
    "$$\n",
    "\n",
    "and \n",
    "\n",
    "$$\n",
    "\\hat{A} = \\sqrt{\\tfrac{\\hbar c^2}{\\epsilon_0 L^3}}\\sum_{\\alpha=1}^{M_p} \\tfrac{1}{\\sqrt{2 \\tilde{\\omega}_{\\alpha}}} (\\hat{b}_{\\alpha} + \\hat{b}^\\dagger_{\\alpha}),\n",
    "$$\n",
    "\n",
    "with $\\tilde{\\omega}_{\\alpha}$ the new mode frequencies in the partially diagonalized problem. In the case that $\\omega_{\\alpha} \\gg \\omega_{\\text{d}}$ for all $\\alpha$ we can approximate $\\tilde{\\omega}_{\\alpha} = \\sqrt{\\omega_{\\alpha}^2 + \\omega_{\\text{d}}^2}$. This approximation is particularly reasonable in the case we consider a large (free-space) quantization volume since $\\omega_{\\text{d}} = \\sqrt{\\frac{e^2 N_e}{m \\epsilon_0 L^3}}$, with $N_e =1$ in our case.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a935c95e",
   "metadata": {},
   "source": [
    "## Q. 4.1\n",
    "-------------\n",
    "Diagonalize the approximate Hamiltonian $\\hat{H}^{\\text{lwa}}_{\\text{vel}}$. \n",
    "\n",
    "<details>\n",
    "<summary>Hints</summary>\n",
    "\n",
    "Use that all the modes $\\alpha$ couple in the same way to the plane-wave solution of the matter subsystem $\\ket{k_l}$, i.e., try the ansatz\n",
    "\n",
    "$$\n",
    "\\ket{\\Psi} = \\ket{k_l} \\otimes \\ket{\\tilde{m}_1} \\otimes \\dots \\otimes \\ket{\\tilde{m}_{M_p}}, \n",
    "$$\n",
    "\n",
    "with the individually shifted oscillators of <font color='blue'>A 3.1 (Notebook 3)</font>.\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd2a0d2b",
   "metadata": {},
   "source": [
    "## Q. 4.2\n",
    "-------------\n",
    "The individual coupling of the particle to each mode $\\alpha$ is given by $ \\omega_{\\text{d}}^2/\\tilde{\\omega}_{\\alpha}^2$. With the previous assumption $\\omega_{\\alpha} \\gg \\omega_{\\text{d}}$, it would appear that the coupling of the particle to light is negligible. However, the fact that we have to sum over many photonic modes can lead to a substantial mass renormalization, i.e.  \n",
    "\n",
    "$$\n",
    "m_{\\text{ren}} = m(1-\\gamma_{\\text{eff}})^{-1}.\n",
    "$$\n",
    "\n",
    "This is exactly the same expression for the mass renormalization found in <font color='blue'>Notebook 3</font> with the effective coupling instead. We also realize that if $\\gamma_{\\text{eff}} \\rightarrow 1$ we get a diverging renormalized mass.\n",
    "\n",
    "Although we have derived everything for one dimensional massive particles, we can also consider the effect for a single electron in three dimensions. To be specific, we consider the free-space vacuum of the electromagentic field, which is with $\\alpha \\equiv (\\boldsymbol{n}, \\lambda)$\n",
    "\n",
    "$$\n",
    "\\omega_{\\alpha} = \\frac{2 \\pi}{L}c |\\boldsymbol{n}| \\quad {\\rm and} \\quad \\boldsymbol{\\epsilon}_{\\alpha} = \\boldsymbol{\\epsilon}(\\boldsymbol{n}, \\lambda).  \n",
    "$$\n",
    "\n",
    "Since the free-space field is isotropic and for each mode $\\alpha$ we have two transverse polarizations, the corresponding effective coupling becomes\n",
    "\n",
    "$$\n",
    "\\gamma_{\\text{eff}} = \\frac{2}{3} \\sum_{\\alpha=1}^{M_p} \\frac{\\omega_{\\text{d}}^2}{\\omega_{\\alpha}^2 + \\omega_{\\text{d}}^2}, \n",
    "$$\n",
    "\n",
    "if we assume the sum runs over all modes with $\\tfrac{2 \\pi}{L}c|\\boldsymbol{n}| \\leq \\Lambda$ with $\\Lambda$ a maximal (cutoff) wave number. Determine the renormalized mass for $L \\rightarrow \\infty$. For atomic units $m_{\\rm ren} = \\hbar=|e|=4 \\pi \\epsilon_0=1$ plot the bare mass $m$ as a function of the cutoff $\\Lambda$ up until $\\Lambda = 50 000$. What do you observe?  \n",
    "\n",
    "<details>\n",
    "<summary>Hint</summary>\n",
    "\n",
    "Turn the sum into an integral and use $\\frac{(2 \\pi)^3}{L^3} \\rightarrow d \\bold{k}$, use further that $\\omega_{\\text{d}} \\rightarrow 0$ in the denominator and define $\\left(\\tfrac{e^2}{4 \\pi \\hbar c \\epsilon_0}\\right) = \\alpha_{\\text{fs}}$ the fine-structure constant.\n",
    "\n",
    "</details>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
