{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "df9f150a",
   "metadata": {},
   "source": [
    "# Notebook 11 - QEDFT electron-photon exchange approximation for one electron coupled to one photon mode in one dimension \n",
    "-----------------------------------------------------------------------------------------------------------\n",
    "\n",
    "Here we look into a 1D light-matter system that consists of one electron and one photon mode using the QEDFT electron-photon exchange functional. \n",
    "\n",
    "The Kohn-Sham (KS) Hamiltonian is (in the Hartree atomic units)\n",
    "$$\n",
    "\\hat{H}_{\\rm{KS}} = -\\frac{1}{2}\\partial_{x}^{2} + v_{\\rm{ext}}(x) + v_{\\rm{px}}(x),\n",
    "$$\n",
    "where $v_{\\rm{ext}}(x)$ is the external potential and $v_{\\rm{px}}(x)$ is the electron-photon exchange potential. Here no Coulomb interaction is present, because there is only one electron in the system. \n",
    "\n",
    "\n",
    "From the lecture, the electron-photon exchange potential for general cases can be obtained by solving the following Poisson equation, \n",
    "$$\n",
    "\\nabla^{2}v_{{\\rm{px}}}(\\mathbf{r}) = -\\nabla\\cdot\\left[\\sum_{\\alpha=1}^{M_{p}}\\frac{\\tilde{\\lambda}_{\\alpha}^{2}}{2\\tilde{\\omega}_{\\alpha}^{2}}\\frac{(\\tilde{\\mathbf{\\varepsilon}}_{\\alpha}\\cdot\\nabla) \\left(\\mathbf{f}_{\\alpha,\\rm{px}}(\\mathbf{r})+c.c.\\right)}{\\rho(\\mathbf{r})}\\right],\n",
    "$$\n",
    "where $\\mathbf{f}_{\\alpha,\\rm{px}}(\\mathbf{r}) =\\langle(\\tilde{\\boldsymbol{\\varepsilon}}_{\\alpha}\\cdot\\hat{\\mathbf{J}}_{\\rm{p}})\\hat{\\mathbf{j}}_{\\rm{p}}(\\mathbf{r})\\rangle_{\\Phi}$, $\\Phi$ is the Slater-determinant constructed from the KS orbitals, $\\rho(\\mathbf{r})$ is the electron density, $c.c.$ is the complex conjugate, $\\hat{\\mathbf{J}}_{\\rm{p}} = \\sum_{l=1}^{N_{e}}(-i)\\nabla_{l}$, and $\\hat{\\mathbf{j}}_{\\rm{p}}(\\mathbf{r}) = \\frac{1}{2i}\\sum_{l=1}^{N_{e}} \\left(\\delta({\\mathbf{r}_{l}-\\mathbf{r}})\\overrightarrow\\nabla_{l}-\\overleftarrow\\nabla_{l}\\delta({\\mathbf{r}_{l}-\\mathbf{r}})\\right)$.\n",
    "\n",
    "\n",
    "The electron-photon exchange potential within the local-density approximation (LDA) for general cases can be obtained by solving the following Poisson equation, \n",
    "$$\n",
    "\\nabla^{2} v_{{\\rm{pxLDA}}}(\\mathbf{r}) = -\\sum_{\\alpha=1}^{M_{p}}\\frac{2 \\pi^{2}\\tilde{\\lambda}_{\\alpha}^{2}}{\\tilde{\\omega}_{\\alpha}^{2}}\\left[(\\tilde{\\boldsymbol{\\varepsilon}}_{\\alpha}\\cdot\\nabla)^{2}\\left(\\frac{\\rho(\\mathbf{r})}{2V_{d}}\\right)^{\\frac{2}{d}}\\right],\n",
    "$$\n",
    "where $V_{d} = 2, \\pi, 4\\pi/3$ for 1D, 2D, and 3D, respectively. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e1a960b",
   "metadata": {},
   "source": [
    "## Q. 11.1 Derive the electron-photon exchange potential for the one-electron case in 1D\n",
    "-------------\n",
    "\n",
    "Please derive the electron-photon exchange potential for one-electron cases in 1D system, $v_{\\rm{px}}(x)$. \n",
    "\n",
    "<details>\n",
    "<summary>Hint</summary>\n",
    "\n",
    "In one-electron cases, we set the wave function to be real-valued in term of the electron density, $\\phi(\\mathbf{r}) = \\rho^{1/2}(\\mathbf{r})$, and in 1D systems, we set $\\boldsymbol{\\varepsilon}_{\\alpha} =\\hat{e}_{x}$ where $\\hat{e}_{x}$ is the unit vector along the x direction.\n",
    "\n",
    "</details>\n",
    "\n",
    "**(Optional: challenging)** Please derive an analytical expression for the electron-photon exchange potential for one-electrons cases in any dimension, $v_{\\rm{px}}(\\mathbf{r})$. \n",
    "\n",
    "Please also obtain the electron-photon exchange potential with the LDA approximation for one-electron cases in 1D systems, $v_{\\rm{pxLDA}}(x)$. If now we look into an isotropic cases in $d$-dimension, what is the corresponding isotropic electron-photon exchange potential? \n",
    "\n",
    "<details>\n",
    "<summary>Hint</summary>\n",
    "\n",
    "In isotropic cases in $d$-dimension, we can set $({\\boldsymbol{\\varepsilon}}_{\\alpha}\\cdot\\nabla)^{2} = \\frac{1}{d}\\nabla^{2}$.\n",
    "\n",
    "</details>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7673bf03",
   "metadata": {},
   "source": [
    "## Q. 13.2 Warm up: solve a harmonic oscillator \n",
    "------\n",
    "\n",
    "Before solving the KS Hamiltonian of the 1D soft hydrogen atom inside a cavity in a self-consistent way, we use a harmonic oscillator as an example to check our numerics. The analytical wave function for the ground state of a harmonic oscillator is $\\pi^{-1/4}\\exp(-x^{2}/2.0)$. \n",
    "\n",
    "Please write a short program to obtain the (ground-state) electron density of a harmonic oscillator (outside the cavity) and plot the resulting electron density with the analytical result. Please show the two electron densities (one from your code and the other from the analytical one) in the same plot.  \n",
    "\n",
    "The Hamiltonian for the quantum Harmonic oscillator (QHO) is \n",
    "$$\n",
    "\\hat{H}_{\\rm{QHO}} = -\\frac{1}{2}\\partial_{x}^{2} + \\frac{1}{2}x^{2}.\n",
    "$$\n",
    "\n",
    "The following is a python script to help you answer this question, and you just need to fill in the missing lines, which are denoted as `??????` in the script: \n",
    "\n",
    "<details>\n",
    "<summary>Hint</summary>\n",
    "    You could reuse the subroutine developed in <font color='blue'>Notebook 2</font> for the kinetic-energy operator for a free electron in real space basis. Note that here we deal with a finite system, not a periodic system. You could just add a potential in real space basis (diagonal in real space) to the kinetic energy to get the Hamiltonian in the real space (grid) basis.\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a66b348f",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy import special\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Define the plot function\n",
    "def plot_density(x, analytical_solution, numerical_solution):\n",
    "    ts = 20\n",
    "    ls = 20\n",
    "    lgs = 20\n",
    "    lw = 4\n",
    "    ms = 10\n",
    "    mew = 4\n",
    "    nstep = 10\n",
    "\n",
    "    # Plot the numerical and analytical eigenfunctions\n",
    "    plt.figure(figsize=(12, 6))\n",
    "\n",
    "\n",
    "    plt.plot(x,analytical_solution**2, 'k-',lw=lw, label='Analytical')\n",
    "    plt.plot(x[::nstep],numerical_solution[::nstep],\n",
    "             color='r', marker='x', ms=ms, mew= mew, linestyle=\"None\", label='Numerical')\n",
    "\n",
    "    # plt.title('Real Part of Eigenfunctions')\n",
    "    plt.tick_params(which='both', direction='in', labelsize=ts)\n",
    "    plt.xlabel('Position [Bohr]', size=ls)\n",
    "    plt.ylabel(r'Density [Bohr$^{-1}$]', size=ls)\n",
    "    plt.legend(prop={'size':lgs})\n",
    "\n",
    "    plt.tight_layout()  # Adjust layout to prevent overlap\n",
    "    plt.show()\n",
    "\n",
    "# Define the harmonic potential\n",
    "def v_harmonic(x):\n",
    "    return ??????\n",
    "\n",
    "# Define parameters\n",
    "L = 10.0  # Length of the spatial dimension\n",
    "N = 1501  # Number of grid points\n",
    "dx = L / N  # Grid spacing\n",
    "x = np.linspace(0, L, N)  # Spatial grid\n",
    "\n",
    "# Center the grid around the origin\n",
    "x = ??????\n",
    "\n",
    "# Construct the discretized second derivative operator\n",
    "A = ??????\n",
    "\n",
    "# Construct the local potential\n",
    "potential = v_harmonic(x)\n",
    "V = ??????\n",
    "\n",
    "# Construct the Hamiltonian\n",
    "H = A + V\n",
    "\n",
    "# Solve the eigenvalue problem\n",
    "eigenvalues, eigenfunctions = np.linalg.eigh(H)\n",
    "print('First three numerical eigenvalues')\n",
    "print(eigenvalues[:3])\n",
    "\n",
    "# Compute the electron density from the eigenfunction of the ground state\n",
    "rho_0 = ??????\n",
    "\n",
    "# Analytical eigenfunction for the ground state\n",
    "analytical_solution = 1./(np.pi**(1./4.))*np.exp(-x**2/2.0)\n",
    "\n",
    "\n",
    "# plotting the figure\n",
    "plot_density(x, analytical_solution, rho_0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e57c1ee4",
   "metadata": {},
   "source": [
    "## Q. 11.3 Solve the soft hydrogen atom outside a cavity\n",
    "------\n",
    "\n",
    "Once we have the codes to obtain the electron density of the harmonic oscillator outside a cavity, we can simply change the potential of the harmonic oscillator to that of the soft hydrogen atom. \n",
    "\n",
    "The potential for the one-dimensional soft hydrogen atom is \n",
    "$$\n",
    "v_{\\rm{ext}}(x) = v_{\\rm{soft}}(x) = \\frac{-1}{\\sqrt{x^{2}+a^{2}}},\n",
    "$$\n",
    "where $a$ is the softening parameter. We set $a=1.0$ in this case. \n",
    "\n",
    "Please solve the soft hydrogen atom and obtain its electron density, and plot the potential of the soft hydrogen atom with the electron density in the same figure.\n",
    "\n",
    "The following is a unfinished python script, please fill up the missing parts, which are denoted as `??????` in the script: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f54df90f",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy import special\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# define plotting\n",
    "def plot_figure(x, density, potential):\n",
    "    # Parameters for plotting the figure\n",
    "    ts = 20\n",
    "    ls = 20\n",
    "    lgs = 20\n",
    "    lw = 4\n",
    "    ms = 10\n",
    "    mew = 4\n",
    "    nstep = 10\n",
    "    labelpad = 22\n",
    "\n",
    "    plt.figure(figsize=(12, 6))\n",
    "    fig, ax1 = plt.subplots(figsize=(12, 6))\n",
    "    ax2 = ax1.twinx()\n",
    "\n",
    "    ax1.plot(x,rho_0, color='r', linestyle='-', lw=lw)\n",
    "    ax2.plot(x,potential, color='b', linestyle='-', lw=lw);\n",
    "\n",
    "\n",
    "    # plt.title('Real Part of Eigenfunctions')\n",
    "    ax1.tick_params(which='both', direction='in', labelsize=ts)\n",
    "    ax1.tick_params(which='both', axis='y', direction='in', labelsize=ts, colors='r')\n",
    "    ax1.set_xlabel('Position [Bohr]', size=ls)\n",
    "    ax1.set_ylabel(r'Density [Bohr$^{-1}$]', size=ls, color='r')\n",
    "    ax1.set_ylim([-1.1,0.5])\n",
    "\n",
    "    ax2.tick_params(which='both', direction='in', labelsize=ts)\n",
    "    ax2.tick_params(which='both', axis='y', direction='in', labelsize=ts, colors='b')\n",
    "    ax2.set_ylabel(r'Potential [Hatree]', size=ls, color='b', rotation=270, labelpad=labelpad)\n",
    "    ax2.set_ylim([-1.1,0.5])\n",
    "\n",
    "    plt.tight_layout()  # Adjust layout to prevent overlap\n",
    "    plt.show()\n",
    "\n",
    "# Define parameters\n",
    "L = 10.0  # Length of the spatial dimension\n",
    "N = 1501  # Number of grid points\n",
    "dx = L / N  # Grid spacing\n",
    "x = np.linspace(0, L, N)  # Spatial grid\n",
    "\n",
    "# Center the grid around the origin\n",
    "x = x-x[int((N-1)/2)]\n",
    "\n",
    "# Your codes\n",
    "??????\n",
    "\n",
    "# Construct the local potential\n",
    "potential = ??????\n",
    "\n",
    "# Density of the ground state\n",
    "rho_0 = ??????\n",
    "\n",
    "plot_figure(x, rho_0, potential)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9196d6d0",
   "metadata": {},
   "source": [
    "## Q. 11.4 Solve the soft hydrogen atom inside a cavity\n",
    "------\n",
    "\n",
    "Here we are moving on to the more challenging part. Please construct a self-consistent-field program to solve the KS Hamiltonian of the soft-hydrogen atom inside a cavity (with one photon mode) using the electron-photon exchange potential within the LDA. We would like to know how the electron density of the one-electron system changes inside a cavity with one photon mode.  \n",
    "\n",
    "The KS Hamiltonian is \n",
    "$$\n",
    "\\hat{H}_{\\rm{KS}} = -\\frac{1}{2}\\partial_{x}^{2} + v_{\\rm{soft}}(x) + v_{\\rm{pxLDA}}(x),\n",
    "$$\n",
    "where $v_{\\rm{soft}}(x) = \\frac{-1}{\\sqrt{x^{2}+1^{2}}}$ and $v_{\\rm{pxLDA}}(x) = -\\frac{\\pi^{2}}{8}  \\frac{\\lambda_{\\alpha}^{2}}{\\omega_{\\alpha}^{2}+\\lambda_{\\alpha}^{2}} \\rho^{2}(x)$. Here we consider one photon mode with a frequency $\\omega_{\\alpha}$ and a light-matter coupling $\\lambda_{\\alpha}$. Please use $\\omega_{\\alpha} = 1$ and $\\lambda_{\\alpha} =\\{10^{-1}, 1, 10\\}$ to see how the electron density changes, compared to the outside-cavity case. \n",
    "\n",
    "Here are the step-by-step guidelines to help you:\n",
    "\n",
    "- Define a function `solve_H_get_rho(Hamiltonian,dx)` that takes a Hamiltonian and the real-space step and returns the ground-state electron density. Note that you have done this in the previous questions Q. 11.2 and 11.3! In that function, you just need to diagonalize the Hamiltonian and compute the electron density from the groundstate. \n",
    "\n",
    "\n",
    "- Define a function `v_pxlda(rho,omega,lmcpl)` that takes the electron density $\\rho(x)$ and returns the electron-photon exchange potential with the LDA, i.e., $v_{\\rm{pxLDA}}(x) = -\\frac{\\pi^{2}}{8}  \\frac{\\lambda_{\\alpha}^{2}}{\\omega_{\\alpha}^{2}+\\lambda_{\\alpha}^{2}} \\rho^{2}(x)$. Here `rho` the electron density, `omega` the photon frequency $\\omega_{\\alpha}$, and `lmcpl` the light-matter coupling $\\lambda_{\\alpha}$.\n",
    "\n",
    "\n",
    "- Define another function `get_rho_diff(rho_old, rho_new, dx)` that takes the old and next electron density and compute the difference via the following formula, \n",
    "$$\n",
    "\\Delta \\rho = \\int dx \\ |\\rho_{\\rm{new}}(x)-\\rho_{\\rm{old}}(x)|. \n",
    "$$\n",
    "This function is used to check whether the result is converged. \n",
    "\n",
    "\n",
    "- Once you have these three functions, please write a `while` loop to check whether the electron density is converged when $\\Delta \\rho$ is smaller than a convergence condition you give, i.e., `absden = 1e-10`. \n",
    "\n",
    "\n",
    "Plot the difference of the electron density of the soft hydrogen atom inside and outside the cavity, and see how it changes with the light-matter coupling strength, and give an explanation of why the density changes in that way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9ed72fc7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy import special\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Plot the electron density outside and inside the cavity\n",
    "def plot_figure(x,rho_outside,rho_inside)\n",
    "    # Parameters for plotting the figure\n",
    "    ts = 20\n",
    "    ls = 20\n",
    "    lgs = 20\n",
    "    lw = 4\n",
    "    ms = 10\n",
    "    mew = 4\n",
    "    nstep = 10\n",
    "    labelpad = 22\n",
    "\n",
    "    # Plot the numerical and analytical eigenfunctions\n",
    "    plt.figure(figsize=(12, 6))\n",
    "\n",
    "    plt.plot(x,rho_outside, color='k', linestyle='-', lw=lw, label='outside cavity')\n",
    "    plt.plot(x,rho_inside, color='r', linestyle='-', lw=lw, label='inside cavity')\n",
    "\n",
    "    plt.tick_params(which='both', direction='in', labelsize=ts)\n",
    "    plt.xlabel('Position [Bohr]', size=ls)\n",
    "    plt.ylabel(r'Density [Bohr$^{-1}$]', size=ls)\n",
    "    plt.legend(prop={'size':lgs})\n",
    "\n",
    "    plt.tight_layout()  # Adjust layout to prevent overlap\n",
    "    plt.show()\n",
    "\n",
    "\n",
    "# Define the harmonic potential\n",
    "def v_soft(x,a=1):\n",
    "    return ??????\n",
    "\n",
    "# Define the pxLDA potential\n",
    "def v_pxlda(rho,omega,lmcpl):\n",
    "    return ??????\n",
    "\n",
    "# Define solver\n",
    "def solve_H_get_rho(Hamiltonian,dx):\n",
    "    eigenvalues, eigenfunctions = ??????\n",
    "    rho_gs = np.abs(eigenfunctions[:,0])**2/dx\n",
    "    return rho_gs\n",
    "\n",
    "# Define get the rho difference\n",
    "def get_rho_diff(rho_old, rho_new, dx):\n",
    "    sum_abs_drho = ??????\n",
    "    return sum_abs_drho\n",
    "\n",
    "# Define parameters\n",
    "L = 10.0  # Length of the spatial dimension\n",
    "N = 1501  # Number of grid points\n",
    "omega = 1.0\n",
    "lmcpl = 1 #1e-1 #1 #10\n",
    "\n",
    "\n",
    "dx = L / N  # Grid spacing\n",
    "x = np.linspace(0, L, N)  # Spatial grid\n",
    "# Center the grid around the origin\n",
    "x = x-x[int((N-1)/2)]\n",
    "\n",
    "# Construct the discretized second derivative operator\n",
    "A = ??????\n",
    "\n",
    "# Construct the local potential\n",
    "potential = v_soft(x)\n",
    "V = np.zeros((N, N))\n",
    "V = np.diag(potential)\n",
    "\n",
    "# Construct the Hamiltonian\n",
    "H0 = A + V\n",
    "rho_outside = ??????\n",
    "rho_old = np.copy(rho_outside)\n",
    "\n",
    "iteration = 0\n",
    "absden = 1e-10\n",
    "\n",
    "drho = np.sum(np.abs(rho_old))\n",
    "\n",
    "# Self consistent loop\n",
    "??????\n",
    "\n",
    "plot_figure(??????)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
